﻿using BasketballTeam.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballTeam.Models
{
    public class Team : IEntity<int>
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public Conference Conference { get; set; }
        public int FundYear { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }

        [ForeignKey ("Coach")]
        public int CoachID { get; set; }
        public virtual ICollection<Player> Players { get; set; }
        public virtual Coach Coach { get; set; }
    }
}
