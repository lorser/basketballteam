﻿using BasketballTeam.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballTeam.Models
{
    public class Player : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("Team")]
        public int TeamID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public int ShirtNumber { get; set; }
        public string Position { get; set; }
        public Team Team { get; set; }
    }
}
